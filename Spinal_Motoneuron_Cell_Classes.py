# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 16:32:16 2019

Spinal_Motoneuron_Cell_Classes.py

This file contains the Spinal_Motoneuron class describing a five-compartment
motoneuron structure (single soma and four dendritic compartments) each
containing hodgkin-huxley type ionic mechanisms. Each dendritic compartment
receives a synaptic input in the form of a point process.

Parameters are set to be identical for each motoneuron within the pool in this
script. Future work will involve setting unique parameters following a 
left-skewed exponential distribution depending on motoneuron threshold.

@author: Sageanne Senneff, sageanne.senneff@ucdconnect.ie

"""
from neuron import h
from nrnutils import Mechanism, Section
from pyNN.neuron import NativeCellType
from pyNN.parameters import Sequence
import numpy as np

try:
	reduce
except NameError:
	from functools import reduce

def _new_property(obj_hierarchy, attr_name):
	"""
	Returns a new property, mapping attr_name to obj_hierarchy.attr_name.

	For example, suppose that an object of class A has an attribute b which
	itself has an attribute c which itself has an attribute d. Then placing
		e = _new_property('b.c', 'd')
	in the class definition of A makes A.e an alias for A.b.c.d
	"""

	def set(self, value):
		obj = reduce(getattr, [self] + obj_hierarchy.split('.'))
		setattr(obj, attr_name, value)

	def get(self):
		obj = reduce(getattr, [self] + obj_hierarchy.split('.'))
		return getattr(obj, attr_name)
	return property(fset=set, fget=get)

class Spinal_Motoneuron(object):

	def __init__(self, **parameters):
		
		self.soma = Section(diam = parameters['soma_diam'], L = parameters['soma_L'], Ra = parameters['soma_Ra'], cm = parameters['soma_cm'], 
		mechanisms = (Mechanism('na3rp', gbar = parameters['soma_na3rp_gbar'], sh = parameters['soma_na3rp_sh'], ar = parameters['soma_na3rp_ar'], qinf = parameters['soma_na3rp_qinf'], thinf = parameters['soma_na3rp_thinf']), 
					  Mechanism('naps', gbar = parameters['soma_naps_gbar'], sh = parameters['soma_naps_sh'], ar = parameters['soma_naps_ar'], vslope = parameters['soma_naps_vslope'], asvh = parameters['soma_naps_asvh'], bsvh = parameters['soma_naps_bsvh']), 
					  Mechanism('kdrRL', gMax = parameters['soma_kdrRL_gMax'], tmin = parameters['soma_kdrRL_tmin'], taumax = parameters['soma_kdrRL_taumax'], mVh = parameters['soma_kdrRL_mVh']), 
					  Mechanism('mAHP', gcamax = parameters['soma_mAHP_gcamax'], gkcamax = parameters['soma_mAHP_gkcamax'], taur = parameters['soma_mAHP_taur'], mtauca = parameters['soma_mAHP_mtauca'], mvhalfca = parameters['soma_mAHP_mvhalfca'])))
		
		# Insert passive mechanism in soma
		self.soma.insert('pas')
		for seg in self.soma:
			seg.pas.g = parameters['soma_pas_g']
			seg.pas.e = parameters['soma_pas_e']
		
		# Insert h mechanism in soma
		self.soma.insert('gh')
		for seg in self.soma:
			seg.gh.ghbar = parameters['soma_gh_ghbar']
			seg.gh.half = parameters['soma_gh_half']
			seg.gh.htau = parameters['soma_gh_htau']
		
		self.d1 = Section(diam = parameters['d1_diam'], L = parameters['d1_L'], Ra = parameters['d1_Ra'], cm = parameters['d1_cm'], 
							mechanisms = (Mechanism('pas', g = parameters['dendrite_pas_g'], e = parameters['dendrite_pas_e']), 
											Mechanism('gh', ghbar = parameters['dendrite_gh_ghbar'], half = parameters['dendrite_gh_half'], htau = parameters['dendrite_gh_htau'])))
		
		self.d2 = Section(diam = parameters['d2_diam'], L = parameters['d2_L'], Ra = parameters['d2_Ra'], cm = parameters['d2_cm'], 
							mechanisms = (Mechanism('pas', g = parameters['dendrite_pas_g'], e = parameters['dendrite_pas_e']), 
											Mechanism('gh', ghbar = parameters['dendrite_gh_ghbar'], half = parameters['dendrite_gh_half'], htau = parameters['dendrite_gh_htau'])))
				
		self.d3 = Section(diam = parameters['d3_diam'], L = parameters['d3_L'], Ra = parameters['d3_Ra'], cm = parameters['d3_cm'], 
							mechanisms = (Mechanism('pas', g = parameters['dendrite_pas_g'], e = parameters['dendrite_pas_e']), 
											Mechanism('gh', ghbar = parameters['dendrite_gh_ghbar'], half = parameters['dendrite_gh_half'], htau = parameters['dendrite_gh_htau'])))
						
		self.d4 = Section(diam = parameters['d4_diam'], L = parameters['d4_L'], Ra = parameters['d4_Ra'], cm = parameters['d4_cm'], 
							mechanisms = (Mechanism('pas', g = parameters['dendrite_pas_g'], e = parameters['dendrite_pas_e']), 
											Mechanism('gh', ghbar = parameters['dendrite_gh_ghbar'], half = parameters['dendrite_gh_half'], htau = parameters['dendrite_gh_htau'])))

		# Connect the dendritic compartments to their respective parts of the motoneuron soma
		# Note connect only takes one parameter
		self.d1.connect(self.soma(1))
		self.d2.connect(self.soma(1))
		self.d3.connect(self.soma(0))
		self.d4.connect(self.soma(0))

		# Insert the persistent inward currents on the dendrites & set their values
		g_Ca_Max_values = parameters['dendrite_L_Ca_inact_gcabar'].value
		
		# Dendrite 1:
		self.d1.insert('L_Ca_inact')
		for seg in self.d1:
			seg.L_Ca_inact.gcabar = g_Ca_Max_values[0]
			seg.L_Ca_inact.theta_m = parameters['dendrite_L_Ca_inact_theta_m']
			seg.L_Ca_inact.tau_m = parameters['dendrite_L_Ca_inact_tau_m']
			seg.L_Ca_inact.theta_h = parameters['dendrite_L_Ca_inact_theta_h']
			seg.L_Ca_inact.tau_h = parameters['dendrite_L_Ca_inact_tau_h']
			seg.L_Ca_inact.kappa_h = parameters['dendrite_L_Ca_inact_kappa_h']
		
		# Dendrite 2:
		self.d2.insert('L_Ca_inact')
		for seg in self.d2:
			seg.L_Ca_inact.gcabar = g_Ca_Max_values[1]
			seg.L_Ca_inact.theta_m = parameters['dendrite_L_Ca_inact_theta_m']
			seg.L_Ca_inact.tau_m = parameters['dendrite_L_Ca_inact_tau_m']
			seg.L_Ca_inact.theta_h = parameters['dendrite_L_Ca_inact_theta_h']
			seg.L_Ca_inact.tau_h = parameters['dendrite_L_Ca_inact_tau_h']
			seg.L_Ca_inact.kappa_h = parameters['dendrite_L_Ca_inact_kappa_h']
			
		# Dendrite 3:
		self.d3.insert('L_Ca_inact')
		for seg in self.d3:
			seg.L_Ca_inact.gcabar = g_Ca_Max_values[2]
			seg.L_Ca_inact.theta_m = parameters['dendrite_L_Ca_inact_theta_m']
			seg.L_Ca_inact.tau_m = parameters['dendrite_L_Ca_inact_tau_m']
			seg.L_Ca_inact.theta_h = parameters['dendrite_L_Ca_inact_theta_h']
			seg.L_Ca_inact.tau_h = parameters['dendrite_L_Ca_inact_tau_h']
			seg.L_Ca_inact.kappa_h = parameters['dendrite_L_Ca_inact_kappa_h']
			
		# Dendrite 4:
		self.d4.insert('L_Ca_inact')
		for seg in self.d4:
			seg.L_Ca_inact.gcabar = g_Ca_Max_values[3]
			seg.L_Ca_inact.theta_m = parameters['dendrite_L_Ca_inact_theta_m']
			seg.L_Ca_inact.tau_m = parameters['dendrite_L_Ca_inact_tau_m']
			seg.L_Ca_inact.theta_h = parameters['dendrite_L_Ca_inact_theta_h']
			seg.L_Ca_inact.tau_h = parameters['dendrite_L_Ca_inact_tau_h']
			seg.L_Ca_inact.kappa_h = parameters['dendrite_L_Ca_inact_kappa_h']
		
		# John Question - Do the motoneurons need a bias current or is their behaviour solely due to cortical input?
		# Set single point process for descending cortical inputs into the soma
		# for simplicity and debugging (John Template File - Dec 17th)
		self.stim = h.IClamp(0.5, sec=self.soma)
		self.stim.delay = 0
		self.stim.dur = 1e9
		self.stim.amp = 0.0
		
		# Add Excitatory & Inhibitory Exponenetial Synapses to each dendritic compartment
		# Dendrite 1:
		# Excitatory -
		self.d1_exc = h.ExpSyn(0.5, sec=self.d1)
		self.d1_exc.e = parameters['e_rev_exc']				# Excitatory reversal potential
		self.d1_exc.tau = parameters['tau_exc']				# Excitatory time constant
		
		# Inhibitory -
		self.d1_inh = h.ExpSyn(0.5, sec=self.d1)
		self.d1_inh.e = parameters['e_rev_inh']				# Inhibitory reversal potential
		self.d1_inh.tau = parameters['tau_inh']				# Inhibitory time constant
		
		# Dendrite 2:
		# Excitatory -
		self.d2_exc = h.ExpSyn(0.5, sec=self.d2)
		self.d2_exc.e = parameters['e_rev_exc']				# Excitatory reversal potential
		self.d2_exc.tau = parameters['tau_exc']				# Excitatory time constant
		
		# Inhibitory -
		self.d2_inh = h.ExpSyn(0.5, sec=self.d2)
		self.d2_inh.e = parameters['e_rev_inh']				# Inhibitory reversal potential
		self.d2_inh.tau = parameters['tau_inh']				# Inhibitory time constant
		
		# Dendrite 3:
		# Excitatory -
		self.d3_exc = h.ExpSyn(0.5, sec=self.d3)
		self.d3_exc.e = parameters['e_rev_exc']				# Excitatory reversal potential
		self.d3_exc.tau = parameters['tau_exc']				# Excitatory time constant
		
		# Inhibitory -
		self.d3_inh = h.ExpSyn(0.5, sec=self.d3)
		self.d3_inh.e = parameters['e_rev_inh']				# Inhibitory reversal potential
		self.d3_inh.tau = parameters['tau_inh']				# Inhibitory time constant
		
		# Dendrite 4:
		# Excitatory -
		self.d4_exc = h.ExpSyn(0.5, sec=self.d4)
		self.d4_exc.e = parameters['e_rev_exc']				# Excitatory reversal potential
		self.d4_exc.tau = parameters['tau_exc']				# Excitatory time constant
		
		# Inhibitory -
		self.d4_inh = h.ExpSyn(0.5, sec=self.d4)
		self.d4_inh.e = parameters['e_rev_inh']				# Inhibitory reversal potential
		self.d4_inh.tau = parameters['tau_inh']				# Inhibitory time constant
		
		# needed for PyNN
		self.source_section = self.soma
		self.source = self.soma(0.5)._ref_v
		self.rec = h.NetCon(self.source, None, sec=self.source_section)		# Needed to clear the simulator
		self.spike_times = h.Vector(0)
		self.traces = {}
		self.recording_time = False
		self.parameter_names = ('soma_diam', 'soma_L', 'soma_Ra', 'soma_cm', 'soma_ek', 'd1_diam',
								'd1_diam', 'd1_L', 'd1_Ra', 'd1_cm', 
								'd2_diam', 'd2_L', 'd2_Ra', 'd2_cm', 
								'd3_diam', 'd3_L', 'd3_Ra', 'd3_cm', 
								'd4_diam', 'd4_L', 'd4_Ra', 'd4_cm',
								'soma_na3rp_gbar', 'soma_na3rp_sh', 'soma_na3rp_ar', 'soma_na3rp_qinf', 'soma_na3rp_thinf',
								'soma_naps_gbar', 'soma_naps_sh', 'soma_naps_ar', 'soma_naps_vslope', 'soma_naps_asvh', 'soma_naps_bsvh',
								'soma_kdrRL_gMax', 'soma_kdrRL_tmin', 'soma_kdrRL_taumax', 'soma_kdrRL_mVh', 
								'soma_mAHP_gcamax', 'soma_mAHP_gkcamax', 'soma_mAHP_taur', 'soma_mAHP_mtauca', 'soma_mAHP_mvhalfca',
								'soma_gh_ghbar', 'soma_gh_half', 'soma_gh_htau',
								'soma_pas_g', 'soma_pas_e', 
								'dendrite_pas_g', 'dendrite_pas_e',
								'dendrite_L_Ca_inact_gcabar', 'dendrite_L_Ca_inact_theta_m', 'dendrite_L_Ca_inact_tau_m', 'dendrite_L_Ca_inact_theta_h', 'dendrite_L_Ca_inact_tau_h', 'dendrite_L_Ca_inact_kappa_h',
								'dendrite_gh_ghbar', 'dendrite_gh_half', 'dendrite_gh_htau', 
								'e_rev_exc', 'tau_exc', 'e_rev_inh', 'tau_inh'
								)
		self.traces = {}
		self.recording_time = False
		
	def memb_init(self):
		for seg in self.soma:				# Initialize all compartment membrane voltages
			seg.v = self.v_init
		for seg in self.d1:
			seg.v = self.v_init
		for seg in self.d2:
			seg.v = self.v_init
		for seg in self.d3:
			seg.v = self.v_init
		for seg in self.d4:
			seg.v = self.v_init
		
	# Need to make getters and setters for changing motoneuron parameters
	# Soma -
	# na3rp Setters & Getters:
	def _set_soma_na3rp_gbar(self, value):
		for seg in self.soma:
			seg.na3rp.gbar = value
	def _get_soma_na3rp_gbar(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.soma(0.5).na3rp.gbar
	soma_na3rp_gbar = property(fget=_get_soma_na3rp_gbar, fset=_set_soma_na3rp_gbar)
	
	def _set_soma_na3rp_sh(self, value):
		for seg in self.soma:
			seg.na3rp.sh = value
	def _get_soma_na3rp_sh(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.soma(0.5).na3rp.sh
	soma_na3rp_sh = property(fget=_get_soma_na3rp_sh, fset=_set_soma_na3rp_sh)
	
	def _set_soma_na3rp_ar(self, value):
		for seg in self.soma:
			seg.na3rp.ar = value
	def _get_soma_na3rp_ar(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.soma(0.5).na3rp.ar
	soma_na3rp_ar = property(fget=_get_soma_na3rp_ar, fset=_set_soma_na3rp_ar)
	
	def _set_soma_na3rp_qinf(self, value):
		for seg in self.soma:
			seg.na3rp.qinf = value
	def _get_soma_na3rp_qinf(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.soma(0.5).na3rp.qinf
	soma_na3rp_qinf = property(fget=_get_soma_na3rp_qinf, fset=_set_soma_na3rp_qinf)
	
	def _set_soma_na3rp_thinf(self, value):
		for seg in self.soma:
			seg.na3rp.thinf = value
	def _get_soma_na3rp_thinf(self):
		# Return only the middle soma parameter
		return self.soma(0.5).na3rp.thinf
	soma_na3rp_thinf = property(fget=_get_soma_na3rp_thinf, fset=_set_soma_na3rp_thinf)

	
	# naps Setters & Getters:
	def _set_soma_naps_gbar(self, value):
		for seg in self.soma:
			seg.naps.gbar = value
	def _get_soma_naps_gbar(self):
		# Return only the middle soma parameter
		return self.soma(0.5).naps.gbar
	soma_naps_gbar = property(fget=_get_soma_naps_gbar, fset=_set_soma_naps_gbar)

	def _set_soma_naps_sh(self, value):
		for seg in self.soma:
			seg.naps.sh = value
	def _get_soma_naps_sh(self):
		# Return only the middle soma parameter
		return self.soma(0.5).naps.sh
	soma_naps_sh = property(fget=_get_soma_naps_sh, fset=_set_soma_naps_sh)

	def _set_soma_naps_ar(self, value):
		for seg in self.soma:
			seg.naps.ar = value
	def _get_soma_naps_ar(self):
		# Return only the middle soma parameter
		return self.soma(0.5).naps.ar
	soma_naps_ar = property(fget=_get_soma_naps_ar, fset=_set_soma_naps_ar)

	def _set_soma_naps_vslope(self, value):
		for seg in self.soma:
			seg.naps.vslope = value
	def _get_soma_naps_vslope(self):
		# Return only the middle soma parameter
		return self.soma(0.5).naps.vslope
	soma_naps_vslope = property(fget=_get_soma_naps_vslope, fset=_set_soma_naps_vslope)

	def _set_soma_naps_asvh(self, value):
		for seg in self.soma:
			seg.naps.asvh = value
	def _get_soma_naps_asvh(self):
		# Return only the middle soma parameter
		return self.soma(0.5).naps.asvh
	soma_naps_asvh = property(fget=_get_soma_naps_asvh, fset=_set_soma_naps_asvh)

	def _set_soma_naps_bsvh(self, value):
		for seg in self.soma:
			seg.naps.bsvh = value
	def _get_soma_naps_bsvh(self):
		# Return only the middle soma parameter
		return self.soma(0.5).naps.bsvh
	soma_naps_bsvh = property(fget=_get_soma_naps_bsvh, fset=_set_soma_naps_bsvh)

	# kdrRL Setters & Getters:
	def _set_soma_kdrRL_gMax(self, value):
		for seg in self.soma:
			seg.kdrRL.gMax = value
	def _get_soma_kdrRL_gMax(self):
		# Return only the middle soma parameter
		return self.soma(0.5).kdrRL.gMax
	soma_kdrRL_gMax = property(fget=_get_soma_kdrRL_gMax, fset=_set_soma_kdrRL_gMax)

	def _set_soma_kdrRL_tmin(self, value):
		for seg in self.soma:
			seg.kdrRL.tmin = value
	def _get_soma_kdrRL_tmin(self):
		# Return only the middle soma parameter
		return self.soma(0.5).kdrRL.tmin
	soma_kdrRL_tmin = property(fget=_get_soma_kdrRL_tmin, fset=_set_soma_kdrRL_tmin)

	def _set_soma_kdrRL_taumax(self, value):
		for seg in self.soma:
			seg.kdrRL.taumax = value
	def _get_soma_kdrRL_taumax(self):
		# Return only the middle soma parameter
		return self.soma(0.5).kdrRL.taumax
	soma_kdrRL_taumax = property(fget=_get_soma_kdrRL_taumax, fset=_set_soma_kdrRL_taumax)

	def _set_soma_kdrRL_mVh(self, value):
		for seg in self.soma:
			seg.kdrRL.mVh = value
	def _get_soma_kdrRL_mVh(self):
		# Return only the middle soma parameter
		return self.soma(0.5).kdrRL.mVh
	soma_kdrRL_mVh = property(fget=_get_soma_kdrRL_mVh, fset=_set_soma_kdrRL_mVh)

	
	# mAHP Setters & Getters:
	def _set_soma_mAHP_gcamax(self, value):
		for seg in self.soma:
			seg.mAHP.gcamax = value
	def _get_soma_mAHP_gcamax(self):
		# Return only the middle soma parameter
		return self.soma(0.5).mAHP.gcamax
	soma_mAHP_gcamax = property(fget=_get_soma_mAHP_gcamax, fset=_set_soma_mAHP_gcamax)

	def _set_soma_mAHP_gkcamax(self, value):
		for seg in self.soma:
			seg.mAHP.gkcamax = value
	def _get_soma_mAHP_gkcamax(self):
		# Return only the middle soma parameter
		return self.soma(0.5).mAHP.gkcamax
	soma_mAHP_gkcamax = property(fget=_get_soma_mAHP_gkcamax, fset=_set_soma_mAHP_gkcamax)

	def _set_soma_mAHP_taur(self, value):
		for seg in self.soma:
			seg.mAHP.taur = value
	def _get_soma_mAHP_taur(self):
		# Return only the middle soma parameter
		return self.soma(0.5).mAHP.taur
	soma_mAHP_taur = property(fget=_get_soma_mAHP_taur, fset=_set_soma_mAHP_taur)

	def _set_soma_mAHP_mtauca(self, value):
		for seg in self.soma:
			seg.mAHP.mtauca = value
	def _get_soma_mAHP_mtauca(self):
		# Return only the middle soma parameter
		return self.soma(0.5).mAHP.mtauca
	soma_mAHP_mtauca = property(fget=_get_soma_mAHP_mtauca, fset=_set_soma_mAHP_mtauca)

	def _set_soma_mAHP_mvhalfca(self, value):
		for seg in self.soma:
			seg.mAHP.mvhalfca = value
	def _get_soma_mAHP_mvhalfca(self):
		# Return only the middle soma parameter
		return self.soma(0.5).mAHP.mvhalfca
	soma_mAHP_mvhalfca = property(fget=_get_soma_mAHP_mvhalfca, fset=_set_soma_mAHP_mvhalfca)

	# gh Setters & Getters:
	def _set_soma_gh_ghbar(self, value):
		for seg in self.soma:
			seg.gh.ghbar = value
	def _get_soma_gh_ghbar(self):
		# Return only the middle soma parameter
		return self.soma(0.5).gh.ghbar
	soma_gh_ghbar = property(fget=_get_soma_gh_ghbar, fset=_set_soma_gh_ghbar)

	def _set_soma_gh_half(self, value):
		for seg in self.soma:
			seg.gh.half = value
	def _get_soma_gh_half(self):
		# Return only the middle soma parameter
		return self.soma(0.5).gh.half
	soma_gh_half = property(fget=_get_soma_gh_half, fset=_set_soma_gh_half)

	def _set_soma_gh_htau(self, value):
		for seg in self.soma:
			seg.gh.htau = value
	def _get_soma_gh_htau(self):
		# Return only the middle soma parameter
		return self.soma(0.5).gh.htau
	soma_gh_htau = property(fget=_get_soma_gh_htau, fset=_set_soma_gh_htau)

	# pas Setters & Getters:
	def _set_soma_pas_g(self, value):
		for seg in self.soma:
			seg.pas.g = value
	def _get_soma_pas_g(self):
		# Return only the middle soma parameter
		return self.soma(0.5).pas.g
	soma_pas_g = property(fget=_get_soma_pas_g, fset=_set_soma_pas_g)

	def _set_soma_pas_e(self, value):
		for seg in self.soma:
			seg.pas.e = value
	def _get_soma_pas_e(self):
		# Return only the middle soma parameter
		return self.soma(0.5).pas.e
	soma_pas_e = property(fget=_get_soma_pas_e, fset=_set_soma_pas_e)
	
	# Dendrites -
	# pas mechanism - g value:
	def _set_dendrite_pas_g(self, value):
		# Set each dendrite on the motoneuron to have the same parameter value
		for seg in self.d1:
			seg.pas.g = value
		for seg in self.d2:
			seg.pas.g = value
		for seg in self.d3:
			seg.pas.g = value
		for seg in self.d4:
			seg.pas.g = value
	def _get_dendrite_pas_g(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.d1(0.5).pas.g
	dendrite_pas_g = property(fget=_get_dendrite_pas_g, fset=_set_dendrite_pas_g)

	# pas mechanism - e value:
	def _set_dendrite_pas_e(self, value):
		# Set each dendrite on the motoneuron to have the same parameter value
		for seg in self.d1:
			seg.pas.e = value
		for seg in self.d2:
			seg.pas.e = value
		for seg in self.d3:
			seg.pas.e = value
		for seg in self.d4:
			seg.pas.e = value
	def _get_dendrite_pas_e(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.d1(0.5).pas.e
	dendrite_pas_e = property(fget=_get_dendrite_pas_e, fset=_set_dendrite_pas_e)
	
	# L_Ca_inact mechanism - gcabar value:	
	def _set_dendrite_L_Ca_inact_gcabar(self, sequence_values):
		# Values are different on each dendrite compartment, so array of 4 values are wrapped in a sequence
		gcabar_values = sequence_values.value
		for seg in self.d1:
			seg.L_Ca_inact.gcabar = gcabar_values[0]
		for seg in self.d2:
			seg.L_Ca_inact.gcabar = gcabar_values[1]
		for seg in self.d3:
			seg.L_Ca_inact.gcabar = gcabar_values[2]
		for seg in self.d4:
			seg.L_Ca_inact.gcabar = gcabar_values[3]
	def _get_dendrite_L_Ca_inact_gcabar(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		gcabar_values = Sequence(np.array([self.d1(0.5).L_Ca_inact.gcabar, self.d2(0.5).L_Ca_inact.gcabar, self.d3(0.5).L_Ca_inact.gcabar, self.d4(0.5).L_Ca_inact.gcabar]))
		return gcabar_values
	dendrite_L_Ca_inact_gcabar = property(fget=_get_dendrite_L_Ca_inact_gcabar, fset=_set_dendrite_L_Ca_inact_gcabar)
	
	# L_Ca_inact mechanism - theta_m value:
	def _set_dendrite_L_Ca_inact_theta_m(self, value):
		# Set each dendrite on the motoneuron to have the same parameter value
		for seg in self.d1:
			seg.L_Ca_inact.theta_m = value
		for seg in self.d2:
			seg.L_Ca_inact.theta_m = value
		for seg in self.d3:
			seg.L_Ca_inact.theta_m = value
		for seg in self.d4:
			seg.L_Ca_inact.theta_m = value
	def _get_dendrite_L_Ca_inact_theta_m(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.d1(0.5).L_Ca_inact.theta_m
	dendrite_L_Ca_inact_theta_m = property(fget=_get_dendrite_L_Ca_inact_theta_m, fset=_set_dendrite_L_Ca_inact_theta_m)

	# L_Ca_inact mechanism - tau_m value:
	def _set_dendrite_L_Ca_inact_tau_m(self, value):
		# Set each dendrite on the motoneuron to have the same parameter value
		for seg in self.d1:
			seg.L_Ca_inact.tau_m = value
		for seg in self.d2:
			seg.L_Ca_inact.tau_m = value
		for seg in self.d3:
			seg.L_Ca_inact.tau_m = value
		for seg in self.d4:
			seg.L_Ca_inact.tau_m = value
	def _get_dendrite_L_Ca_inact_tau_m(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.d1(0.5).L_Ca_inact.tau_m
	dendrite_L_Ca_inact_tau_m = property(fget=_get_dendrite_L_Ca_inact_tau_m, fset=_set_dendrite_L_Ca_inact_tau_m)

	# L_Ca_inact mechanism - theta_h value:
	def _set_dendrite_L_Ca_inact_theta_h(self, value):
		# Set each dendrite on the motoneuron to have the same parameter value
		for seg in self.d1:
			seg.L_Ca_inact.theta_h = value
		for seg in self.d2:
			seg.L_Ca_inact.theta_h = value
		for seg in self.d3:
			seg.L_Ca_inact.theta_h = value
		for seg in self.d4:
			seg.L_Ca_inact.theta_h = value
	def _get_dendrite_L_Ca_inact_theta_h(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.d1(0.5).L_Ca_inact.theta_h
	dendrite_L_Ca_inact_theta_h = property(fget=_get_dendrite_L_Ca_inact_theta_h, fset=_set_dendrite_L_Ca_inact_theta_h)
	
	# L_Ca_inact mechanism - tau_h value:
	def _set_dendrite_L_Ca_inact_tau_h(self, value):
		# Set each dendrite on the motoneuron to have the same parameter value
		for seg in self.d1:
			seg.L_Ca_inact.tau_h = value
		for seg in self.d2:
			seg.L_Ca_inact.tau_h = value
		for seg in self.d3:
			seg.L_Ca_inact.tau_h = value
		for seg in self.d4:
			seg.L_Ca_inact.tau_h = value
	def _get_dendrite_L_Ca_inact_tau_h(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.d1(0.5).L_Ca_inact.tau_h
	dendrite_L_Ca_inact_tau_h = property(fget=_get_dendrite_L_Ca_inact_tau_h, fset=_set_dendrite_L_Ca_inact_tau_h)
	
	# L_Ca_inact mechanism - kappa_h value:
	def _set_dendrite_L_Ca_inact_kappa_h(self, value):
		# Set each dendrite on the motoneuron to have the same parameter value
		for seg in self.d1:
			seg.L_Ca_inact.kappa_h = value
		for seg in self.d2:
			seg.L_Ca_inact.kappa_h = value
		for seg in self.d3:
			seg.L_Ca_inact.kappa_h = value
		for seg in self.d4:
			seg.L_Ca_inact.kappa_h = value
	def _get_dendrite_L_Ca_inact_kappa_h(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.d1(0.5).L_Ca_inact.kappa_h
	dendrite_L_Ca_inact_kappa_h = property(fget=_get_dendrite_L_Ca_inact_kappa_h, fset=_set_dendrite_L_Ca_inact_kappa_h)
	
	# gh mechanism - ghbar value:
	def _set_dendrite_gh_ghbar(self, value):
		# Set each dendrite on the motoneuron to have the same parameter value
		for seg in self.d1:
			seg.gh.ghbar = value
		for seg in self.d2:
			seg.gh.ghbar = value
		for seg in self.d3:
			seg.gh.ghbar = value
		for seg in self.d4:
			seg.gh.ghbar = value
	def _get_dendrite_gh_ghbar(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.d1(0.5).gh.ghbar
	dendrite_gh_ghbar = property(fget=_get_dendrite_gh_ghbar, fset=_set_dendrite_gh_ghbar)
	
	# gh mechanism - half value:
	def _set_dendrite_gh_half(self, value):
		# Set each dendrite on the motoneuron to have the same parameter value
		for seg in self.d1:
			seg.gh.half = value
		for seg in self.d2:
			seg.gh.half = value
		for seg in self.d3:
			seg.gh.half = value
		for seg in self.d4:
			seg.gh.half = value
	def _get_dendrite_gh_half(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.d1(0.5).gh.half
	dendrite_gh_half = property(fget=_get_dendrite_gh_half, fset=_set_dendrite_gh_half)
	
	# gh mechanism - htau value:
	def _set_dendrite_gh_htau(self, value):
		# Set each dendrite on the motoneuron to have the same parameter value
		for seg in self.d1:
			seg.gh.htau = value
		for seg in self.d2:
			seg.gh.htau = value
		for seg in self.d3:
			seg.gh.htau = value
		for seg in self.d4:
			seg.gh.htau = value
	def _get_dendrite_gh_htau(self):
		# Return only the first dendrite parameter, since other dendrites use same value
		return self.d1(0.5).gh.htau
	dendrite_gh_htau = property(fget=_get_dendrite_gh_htau, fset=_set_dendrite_gh_htau)
	
	# Getters and Setters for dendritic synapses
	def _get_e_rev_exc(self):
		return self.d1_exc.e
	def _set_e_rev_exc(self, value):
		self.d1_exc.e = value
		self.d2_exc.e = value
		self.d3_exc.e = value
		self.d4_exc.e = value
	e_rev_exc = property(fget=_get_e_rev_exc, fset=_set_e_rev_exc)

	def _get_e_rev_inh(self):
		return self.d1_inh.e
	def _set_e_rev_inh(self, value):
		self.d1_inh.e = value
		self.d2_inh.e = value
		self.d3_inh.e = value
		self.d4_inh.e = value
	e_rev_inh = property(fget=_get_e_rev_inh, fset=_set_e_rev_inh)
	
	def _get_tau_exc(self):
		return self.d1_exc.tau
	def _set_tau_exc(self, value):
		self.d1_exc.tau = value
		self.d2_exc.tau = value
		self.d3_exc.tau = value
		self.d4_exc.tau = value
	tau_exc = property(fget=_get_tau_exc, fset=_set_tau_exc)

	def _get_tau_inh(self):
		return self.d1_inh.tau
	def _set_tau_inh(self, value):
		self.d1_inh.tau = value
		self.d2_inh.tau = value
		self.d3_inh.tau = value
		self.d4_inh.tau = value
	tau_inh = property(fget=_get_tau_inh, fset=_set_tau_inh)

class Spinal_Motoneuron_Type(NativeCellType):
	
	# Default parameters for the neurons:
	default_parameters = {'soma_diam': 22.0, 'soma_L': 2952.0, 'soma_Ra': 0.001, 'soma_cm': 1.35546, 'soma_ek': -80.0, 'd1_diam': 8.73071,
							'd1_diam': 8.73071, 'd1_L': 1794.13, 'd1_Ra': 51.038, 'd1_cm': 0.867781, 
							'd2_diam': 8.73071, 'd2_L': 1794.13, 'd2_Ra': 51.038, 'd2_cm': 0.867781, 
							'd3_diam': 8.73071, 'd3_L': 1794.13, 'd3_Ra': 51.038, 'd3_cm': 0.867781, 
							'd4_diam': 8.73071, 'd4_L': 1794.13, 'd4_Ra': 51.038, 'd4_cm': 0.867781}
	# Soma default parameters:
	# na3rp:
	default_parameters['soma_na3rp_gbar'] = 0.01
	default_parameters['soma_na3rp_sh'] = 8.0
	default_parameters['soma_na3rp_ar'] = 1.0
	default_parameters['soma_na3rp_qinf'] = 4.0
	default_parameters['soma_na3rp_thinf'] = -50.0
	
	# naps:
	default_parameters['soma_naps_gbar'] = 0.0052085
	default_parameters['soma_naps_sh'] = 0.0
	default_parameters['soma_naps_ar'] = 1.0
	default_parameters['soma_naps_vslope'] = 6.8
	default_parameters['soma_naps_asvh'] = -85.0
	default_parameters['soma_naps_bsvh'] = -17.0
	
	# kdrRL:
	default_parameters['soma_kdrRL_gMax'] = 0.1
	default_parameters['soma_kdrRL_tmin'] = 1.4
	default_parameters['soma_kdrRL_taumax'] = 11.9
	default_parameters['soma_kdrRL_mVh'] = -25.0
	
	# mAHP:
	default_parameters['soma_mAHP_gcamax'] = 3e-05
	default_parameters['soma_mAHP_gkcamax'] = 0.03
	default_parameters['soma_mAHP_taur'] = 20.0
	default_parameters['soma_mAHP_mtauca'] = 1.0
	default_parameters['soma_mAHP_mvhalfca'] = -30.0
	
	# gh:
	default_parameters['soma_gh_ghbar'] = 0.001
	default_parameters['soma_gh_half'] = -80.0
	default_parameters['soma_gh_htau'] = 50.0
	
	# pas:
	default_parameters['soma_pas_g'] = 8.11e-05
	default_parameters['soma_pas_e'] = -71.0
	
	# Dendrite default parameters:
	# pas:
	default_parameters['dendrite_pas_g'] = 7.93e-05
	default_parameters['dendrite_pas_e'] = -71.0
	
	# L_Ca_inact:
	default_parameters['dendrite_L_Ca_inact_gcabar'] = Sequence(np.array([1.01e-5, 1.01e-5, 1.01e-5, 1.01e-5]))
	default_parameters['dendrite_L_Ca_inact_theta_m'] = -42.0
	default_parameters['dendrite_L_Ca_inact_tau_m'] = 40.0
	default_parameters['dendrite_L_Ca_inact_theta_h'] = 10.0
	default_parameters['dendrite_L_Ca_inact_tau_h'] = 2500.0
	default_parameters['dendrite_L_Ca_inact_kappa_h'] = 5.0
	
	# gh:
	default_parameters['dendrite_gh_ghbar'] = 0.001
	default_parameters['dendrite_gh_half'] = -77.0
	default_parameters['dendrite_gh_htau'] = 50.0

	# Dendritic synaptic parameters
	default_parameters['e_rev_exc'] = 0.0
	default_parameters['tau_exc'] = 0.0
	default_parameters['e_rev_inh'] = 0.0
	default_parameters['tau_inh'] = 0.0
	
	default_initial_values = {'v': -65.0}			# Can record soma voltage and synaptic currents on each dendrite
	recordable = ['soma(0.5).v', 'd1_exc.i', 'd1_inh.i', 'd2_exc.i', 'd2_inh.i', 'd3_exc.i', 'd3_inh.i', 'd4_exc.i', 'd4_inh.i']
	units = {'soma(0.5).v' : 'mV', 'd1_exc.i': 'nA', 'd1_inh.i': 'nA', 'd2_exc.i': 'nA', 'd2_inh.i': 'nA', 'd3_exc.i': 'nA', 'd3_inh.i': 'nA', 'd4_exc.i': 'nA', 'd4_inh.i': 'nA'}    
	receptor_types = ['d1_exc', 'd1_inh', 'd2_exc', 'd2_inh', 'd3_exc', 'd3_inh', 'd4_exc', 'd4_inh']
	model = Spinal_Motoneuron
