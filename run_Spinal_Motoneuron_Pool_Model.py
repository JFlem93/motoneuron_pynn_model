# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 14:00:26 2019

run_Spinal_Motoneuron_Pool_Model.py

This is the main run file to simulate a pool of 100 motoneurons within the
spinal cord. Three cortical populations are projected onto each motoneuron
to send excitatory, inhibitory, and correlated synaptic inputs.

@author: Sageanne
"""

import neuron
h = neuron.h

import numpy as np
from pyNN.parameters import Sequence
from pyNN.neuron import setup, end, run, reset, run_until, simulator, Population, SpikeSourceArray, StaticSynapse, Projection, FromFileConnector, OneToOneConnector
from pyNN.random import RandomDistribution, NumpyRNG
from Spinal_Motoneuron_Cell_Classes import Spinal_Motoneuron_Type
from Spinal_Motoneuron_Parameters import Gather_Parameters

def generate_poisson_spike_times(pop_size, start_time, duration, fr, timestep, random_seed):
	""" generate_population_spike_times generates (N = pop_size) poisson distributed spiketrains
		with firing rate fr.
	"""
	#pop_size = 10
	#start_time = 0.0
	#end_time = 6000.0
	#timestep = 1  # ms
	#fr = 1

	# Convert to sec for calculating the spikes matrix
	dt = float(timestep)/1000.0                         # sec
	tSim = float(((start_time+duration) - start_time)/1000.0)         # sec
	nBins = int(np.floor(tSim/dt))

	spikeMat = np.where(np.random.uniform(0,1,(pop_size, nBins)) < fr*dt)

	# Create time vector - ms
	tVec = np.arange(start_time, start_time+duration, timestep)

	# Make array of spike times
	for neuron_index in np.arange(pop_size):
		neuron_spike_times = tVec[spikeMat[1][np.where(spikeMat[0][:]==neuron_index)]]
		if neuron_index == 0:
			spike_times = Sequence(neuron_spike_times)
		else:
			spike_times = np.vstack((spike_times, Sequence(neuron_spike_times)))

	return spike_times

def generate_sync_spike_times(pop_size, start_time, duration, isi, offset, random_seed):
	""" generate_population_spike_times generates (N = pop_size) spiketrains
		of frequency 1/isi. Randomness is added to the isi times so the
		generated spiketrains are not completely synchronous.
	"""
	
	# First - Create trains of synchronous - a spike every isi ms
	spike_times = Sequence(np.arange(start_time, start_time+duration, isi) + offset)
	sync_spike_times = spike_times.value
	num_spikes = len(sync_spike_times)
	spike_time_random = RandomDistribution('uniform',(-(float(isi)*0.3), float(isi)*0.3), rng=NumpyRNG(seed=random_seed))

	# Add randomness
	for i in range(0,pop_size):
		new_spike_time = sync_spike_times + spike_time_random.next(num_spikes)
		
		if new_spike_time[0]<start_time:
			new_spike_time[0] = new_spike_time[0] + (float(isi)*0.3)
		
		if i==0:
			spike_times = Sequence(np.round(new_spike_time))
		else:
			spike_times = np.vstack((spike_times, Sequence(np.round(new_spike_time))))
	
	return spike_times


def concatanate_spike_time_sequences(spike_times_1, spike_times_2):
	""" concatanate two arrays of spike train sequences
	"""
	
	if len(spike_times_1)==len(spike_times_2):
		
		# Loop over the spike trains
		for i in range(0, len(spike_times_1)):
			if i==0:
				#print(spike_times_1[i])
				#print(spike_times_2[i])
				#spike_times = np.hstack((spike_times_1[i][0].value, spike_times_2[i][0].value))
				spike_times = np.hstack((spike_times_1[i][0].value, spike_times_2[i][0].value))
				spike_times = np.sort(spike_times)
				new_spike_times = Sequence(spike_times)
			else:
				#spike_times = np.hstack((spike_times_1[i][0].value, spike_times_2[i][0].value))
				spike_times = np.hstack((spike_times_1[i][0].value, spike_times_2[i][0].value))
				spike_times = np.sort(spike_times)
				new_spike_times = np.vstack((new_spike_times, Sequence(spike_times)))
	else:
		# Print error message
		pass
	
	return new_spike_times

if __name__ == '__main__':
	
    # Setup simulation
    setup(timestep = 0.025)
    simulation_runtime = 2000.0
    simulation_duration = simulation_runtime + simulator.state.dt
    Pop_size = 100
    
	# Use CVode for fast calculation
    cvode = h.CVode()
    cvode.active(0)
	
	# Initial cell membrane voltages
    v_init = -65
	
	# Generate example cortical spike trains:
    cortical_firing_rate_1 = 15
    cortical_firing_rate_2 = 25
    cortical_firing_rate_3 = 10
    cortical_spike_times_1 = generate_sync_spike_times(Pop_size, 1, 6000, 1000.0/cortical_firing_rate_1, 0, 3695)
    cortical_spike_times_2 = generate_sync_spike_times(Pop_size, 1, 6000, 1000.0/cortical_firing_rate_2, 0, 3695)
    cortical_spike_times_3 = generate_poisson_spike_times(Pop_size, 1, 6000, cortical_firing_rate_3, simulator.state.dt, 3695)
	
	# Generate the neuron populations - Need to create the populations before you can assign there values/connections
    Cortical_Pop_Correlated = Population(Pop_size, SpikeSourceArray(spike_times=cortical_spike_times_1[0][0]), label='Correlated Input Cortical Neuron Pop')
    Cortical_Pop_Excitatory = Population(Pop_size, SpikeSourceArray(spike_times=cortical_spike_times_2[0][0]), label='Excitatory Input Cortical Neuron Pop')
    Cortical_Pop_Inhibitory = Population(Pop_size, SpikeSourceArray(spike_times=cortical_spike_times_3[0][0]), label='Inhibitory Input Cortical Neuron Pop')
    Motoneuron_Pop = Population(Pop_size, Spinal_Motoneuron_Type(), initial_values = {'v': v_init}, label = 'Spinal Motoneuron')
	
	# Now update the spike times for each cortical populations
    for i in range(0,Pop_size):
        Cortical_Pop_Correlated[i].spike_times=cortical_spike_times_1[i][0]
		
    for i in range(0,Pop_size):
        Cortical_Pop_Excitatory[i].spike_times=cortical_spike_times_2[i][0]
	
    for i in range(0,Pop_size):
        Cortical_Pop_Inhibitory[i].spike_times=cortical_spike_times_3[i][0]
	
	# Update population parameters
    runner = Gather_Parameters()
    soma_diam, soma_L, soma_g_pas, soma_e_pas, soma_gbar_na3rp, soma_gbar_naps, soma_gMax_kdrRL, soma_gcamax_mAHP, soma_gkcamax_mAHP, soma_taur_mAHP, \
    soma_cm, soma_ghbar_gh, dend_all_L, dend_all_diam, dend_all_g_pas, dend_all_e_pas, dend_all_Ra, dend_all_cm, dend_all_ghbar_gh, dend_1_gcabar_L_Ca_inact, \
    dend_2_gcabar_L_Ca_inact, dend_3_gcabar_L_Ca_inact, dend_4_gcabar_L_Ca_inact, global_theta_m_L_Ca_inact, global_theta_h_L_Ca_inact = runner.__main__()
    
    PIC_Conductances = list()
    for i in range(Pop_size):
        Wrap_Conductances = Sequence(np.array([dend_1_gcabar_L_Ca_inact[i], dend_2_gcabar_L_Ca_inact[i], dend_3_gcabar_L_Ca_inact[i], dend_4_gcabar_L_Ca_inact[i]]))
        PIC_Conductances.append(Wrap_Conductances)
        
    # Assign PIC conductance values to dendrite compartments
    for ii, motoneuron_cell in enumerate(Motoneuron_Pop):
        motoneuron_cell.dendrite_L_Ca_inact_gcabar = PIC_Conductances[ii]
    
    # 2) Set a List of Values for the Population
    # Soma
    Motoneuron_Pop.set(soma_diam=soma_diam)
    Motoneuron_Pop.set(soma_L=soma_L)
    Motoneuron_Pop.set(soma_pas_g=soma_g_pas)
    Motoneuron_Pop.set(soma_pas_e=soma_e_pas)
    Motoneuron_Pop.set(soma_na3rp_gbar=soma_gbar_na3rp)
    Motoneuron_Pop.set(soma_naps_gbar=soma_gbar_naps)
    Motoneuron_Pop.set(soma_kdrRL_gMax=soma_gMax_kdrRL)
    Motoneuron_Pop.set(soma_mAHP_gcamax=soma_gcamax_mAHP)
    Motoneuron_Pop.set(soma_mAHP_gkcamax=soma_gkcamax_mAHP)
    Motoneuron_Pop.set(soma_mAHP_taur=soma_taur_mAHP)
    Motoneuron_Pop.set(soma_cm=soma_cm)
    Motoneuron_Pop.set(soma_gh_ghbar=soma_ghbar_gh)
    # Dendrites
    Motoneuron_Pop.set(d1_L=dend_all_L)
    Motoneuron_Pop.set(d2_L=dend_all_L)
    Motoneuron_Pop.set(d3_L=dend_all_L)
    Motoneuron_Pop.set(d4_L=dend_all_L)
    Motoneuron_Pop.set(d1_diam=dend_all_diam)
    Motoneuron_Pop.set(d2_diam=dend_all_diam)
    Motoneuron_Pop.set(d3_diam=dend_all_diam)
    Motoneuron_Pop.set(d4_diam=dend_all_diam)
    Motoneuron_Pop.set(dendrite_pas_g=dend_all_g_pas)
    Motoneuron_Pop.set(dendrite_pas_e=dend_all_e_pas)
    Motoneuron_Pop.set(d1_Ra=dend_all_Ra)
    Motoneuron_Pop.set(d2_Ra=dend_all_Ra)
    Motoneuron_Pop.set(d3_Ra=dend_all_Ra)
    Motoneuron_Pop.set(d4_Ra=dend_all_Ra)
    Motoneuron_Pop.set(d1_cm=dend_all_cm)
    Motoneuron_Pop.set(d2_cm=dend_all_cm)
    Motoneuron_Pop.set(d3_cm=dend_all_cm)
    Motoneuron_Pop.set(d4_cm=dend_all_cm)
    Motoneuron_Pop.set(dendrite_gh_ghbar=dend_all_ghbar_gh)
    Motoneuron_Pop.set(dendrite_L_Ca_inact_theta_m=global_theta_m_L_Ca_inact)
    Motoneuron_Pop.set(dendrite_L_Ca_inact_theta_h=global_theta_h_L_Ca_inact)
	
	# Synaptic Connections - by changing the weight parameter here you can vary how much of an effect each spike will have on the postsynaptic neurons
    syn_CorticalSpinal_Corr = StaticSynapse(weight = 0.01, delay = 1)
    syn_CorticalSpinal_Exc = StaticSynapse(weight = 0.01, delay = 1)
    syn_CorticalSpinal_Inh = StaticSynapse(weight = 0.01, delay = 1)

	# Generate the synaptic projections from the cortical population to motoneuron population
	# Note - Because each cortical neuron projects to all 4 dendritic compartments on each motoneuron, for simplicity, I am just making 4 different projection objects which will project
	# 		 to each dendritic synapse 
	# Correlated input projections:
    prj_CorticalSpinal_Correlated_1 = Projection(Cortical_Pop_Correlated, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Corr, source='soma(0.5)', receptor_type='d1_exc')	
    prj_CorticalSpinal_Correlated_2 = Projection(Cortical_Pop_Correlated, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Corr, source='soma(0.5)', receptor_type='d2_exc')	
    prj_CorticalSpinal_Correlated_3 = Projection(Cortical_Pop_Correlated, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Corr, source='soma(0.5)', receptor_type='d3_exc')	
    prj_CorticalSpinal_Correlated_4 = Projection(Cortical_Pop_Correlated, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Corr, source='soma(0.5)', receptor_type='d4_exc')	
	
	# Excitatory input projections:
    prj_CorticalSpinal_Excitatory_1 = Projection(Cortical_Pop_Excitatory, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Exc, source='soma(0.5)', receptor_type='d1_exc')	
    prj_CorticalSpinal_Excitatory_2 = Projection(Cortical_Pop_Excitatory, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Exc, source='soma(0.5)', receptor_type='d2_exc')	
    prj_CorticalSpinal_Excitatory_3 = Projection(Cortical_Pop_Excitatory, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Exc, source='soma(0.5)', receptor_type='d3_exc')	
    prj_CorticalSpinal_Excitatory_4 = Projection(Cortical_Pop_Excitatory, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Exc, source='soma(0.5)', receptor_type='d4_exc')	
	
	# Inhibitory input projections:
    prj_CorticalSpinal_Inhibitory_1 = Projection(Cortical_Pop_Inhibitory, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Inh, source='soma(0.5)', receptor_type='d1_inh')	
    prj_CorticalSpinal_Inhibitory_2 = Projection(Cortical_Pop_Inhibitory, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Inh, source='soma(0.5)', receptor_type='d2_inh')	
    prj_CorticalSpinal_Inhibitory_3 = Projection(Cortical_Pop_Inhibitory, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Inh, source='soma(0.5)', receptor_type='d3_inh')	
    prj_CorticalSpinal_Inhibitory_4 = Projection(Cortical_Pop_Inhibitory, Motoneuron_Pop, OneToOneConnector(), syn_CorticalSpinal_Inh, source='soma(0.5)', receptor_type='d4_inh')	
    

	# Record state variables from the populations
	# Recording the spikes from the spike source populations
    Cortical_Pop_Correlated.record('spikes')
    Cortical_Pop_Excitatory.record('spikes')
    Cortical_Pop_Inhibitory.record('spikes')
	
	# Recording the soma membrane voltage and dendritic postsynaptic currents
    Motoneuron_Pop.record('soma(0.5).v')
    Motoneuron_Pop.record('d1_exc.i')
    Motoneuron_Pop.record('d1_inh.i')
    Motoneuron_Pop.record('d2_exc.i')
    Motoneuron_Pop.record('d2_inh.i')
    Motoneuron_Pop.record('d3_exc.i')
    Motoneuron_Pop.record('d3_inh.i')
    Motoneuron_Pop.record('d4_exc.i')
    Motoneuron_Pop.record('d4_inh.i')

	# Run the model - Change and implement a for loop here for force feedback
    run_until(simulation_duration)
	
	# Write data to file
    Motoneuron_Pop.write_data("motoneuron_voltages.mat", 'soma(0.5).v', clear = True)
	
    print("Simulation Done!")
    end()

