# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 14:55:22 2020

This function creates an array of parameter values for each motoneuron within
the motoneuron pool describing the first dorsal interosseous (FDI) muscle. 
Each parameter is given a lower bound and an upper bound, and the interpolation
follows a nonlinear pattern developed by Powers et. al (2017) that is controlled
by the parameter nonlin_ind, setting the steepness of the exponential left skew,
such that the majority of motoneurons within the pool are low-medium threshold.

@author: Sageanne
"""
import numpy as np
from scipy import interpolate

class Gather_Parameters():
    
    def __init__(self):
        
        ## Set number of MUs in Pool
        self.no_mu = 100

        ## Set Steepness of Skew for Nonlinear Interpolation
        self.nonlin_ind = 1.0

        ## Set Lower and Upper Bounds for Parameters   
        
        # Geomtrical Parameters (Soma)
        self.soma_diam_lb = 22
        self.soma_diam_ub = 28

        self.soma_L_lb = 2952
        self.soma_L_ub = 2975.74
        
        self.soma_cm_lb = 1.35546
        self.soma_cm_ub = 1.87853

        # Ionic Channel Parameters (Soma)
        self.soma_g_pas_lb = 8.11e-05
        self.soma_g_pas_ub = 3.77e-04

        self.soma_e_pas_lb = -71
        self.soma_e_pas_ub = -72

        self.soma_gbar_na3rp_lb = 0.01
        self.soma_gbar_na3rp_ub = 0.022

        self.soma_gbar_naps_lb = 2.60e-05
        self.soma_gbar_naps_ub = 2.00e-05

        self.soma_gMax_kdrRL_lb = 0.015
        self.soma_gMax_kdrRL_ub = 0.02

        self.soma_gcamax_mAHP_lb = 4.80e-06
        self.soma_gcamax_mAHP_ub = 7.90e-06
        
        self.soma_gkcamax_mAHP_lb = 0.00045
        self.soma_gkcamax_mAHP_ub = 0.0006

        self.soma_taur_mAHP_lb = 90
        self.soma_taur_mAHP_ub = 30

        self.soma_ghbar_gh_lb = 3.00e-05
        self.soma_ghbar_gh_ub = 2.30e-04
        
        # Geometrical Parameters (Dendrites)
        self.dend_all_L_lb = 1435.3
        self.dend_all_L_ub = 1559.14
        
        self.dend_all_diam_lb = 8.73071
        self.dend_all_diam_ub = 9.05

        self.dend_all_g_pas_lb = 7.93e-05
        self.dend_all_g_pas_ub = 1.75e-04
        
        self.dend_all_e_pas_lb = -71
        self.dend_all_e_pas_ub = -72
        
        self.dend_all_Ra_lb = 51.038
        self.dend_all_Ra_ub = 40.755
        
        self.dend_all_cm_lb = 0.867781
        self.dend_all_cm_ub = 0.880407
        
        # Ionic Channel Parameters (Dendrites)
        self.dend_all_ghbar_gh_lb = 3.00e-05
        self.dend_all_ghbar_gh_ub = 2.30e-04
        
        self.dend_1_gcabar_L_Ca_inact_lb = 8.63e-06
        self.dend_1_gcabar_L_Ca_inact_ub = 1.12e-05
        
        self.dend_2_gcabar_L_Ca_inact_lb = 7.58e-06
        self.dend_2_gcabar_L_Ca_inact_ub = 1.31e-05

        self.dend_3_gcabar_L_Ca_inact_lb = 1.02e-05
        self.dend_3_gcabar_L_Ca_inact_ub = 1.43e-05
        
        self.dend_4_gcabar_L_Ca_inact_lb = 1.19e-05
        self.dend_4_gcabar_L_Ca_inact_ub = 1.58e-05
        
        self.global_theta_m_L_Ca_inact_lb = -42
        self.global_theta_m_L_Ca_inact_ub = -12

        self.global_theta_h_L_Ca_inact_lb = 10
        self.global_theta_h_L_Ca_inact_ub = -10
        

    ## Set Up Each Parameter to be Interpolated
    def soma_diam(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def soma_L(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def soma_g_pas(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def soma_e_pas(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def soma_gbar_na3rp(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def soma_gbar_naps(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param   

    def soma_gMax_kdrRL(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def soma_gcamax_mAHP(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def soma_gkcamax_mAHP(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def soma_taur_mAHP(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def soma_cm(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def soma_ghbar_gh(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def dend_all_L(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def dend_all_diam(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def dend_all_g_pas(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def dend_all_e_pas(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def dend_all_Ra(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def dend_all_cm(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def dend_all_ghbar_gh(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def dend_1_gcabar_L_Ca_inact(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def dend_2_gcabar_L_Ca_inact(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def dend_3_gcabar_L_Ca_inact(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def dend_4_gcabar_L_Ca_inact(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def global_theta_m_L_Ca_inact(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param
    
    def global_theta_h_L_Ca_inact(self, lb, ub, skew):
        param = np.zeros([self.no_mu])
        for i in range(self.no_mu):
            param[i] = lb + (ub-lb)*(skew[i])
        return param

    def __main__(self):
        
        ## Set Up Nonlinear Interpolation Function
        fraction = np.zeros([self.no_mu])
        for cur_cell_no in range(self.no_mu):
            fraction[cur_cell_no] = float(cur_cell_no)/(self.no_mu - 1)
            
        if self.nonlin_ind >= 0:
            skew = fraction**(1 + self.nonlin_ind)
        else:
            skew = 1 - (1-fraction)**(1 - self.nonlin_ind)
            
        ## Calculate Pool Parameters
        # Soma Parameters
        soma_diam = self.soma_diam(self.soma_diam_lb, self.soma_diam_ub, skew)
        soma_L = self.soma_L(self.soma_L_lb, self.soma_L_ub, skew)
        soma_g_pas = self.soma_g_pas(self.soma_g_pas_lb, self.soma_g_pas_ub, skew)
        soma_e_pas = self.soma_e_pas(self.soma_e_pas_lb, self.soma_e_pas_ub, skew)
        soma_gbar_na3rp = self.soma_gbar_na3rp(self.soma_gbar_na3rp_lb, self.soma_gbar_na3rp_ub, skew)
        soma_gbar_naps = self.soma_gbar_naps(self.soma_gbar_naps_lb, self.soma_gbar_na3rp_ub, skew)
        soma_gMax_kdrRL = self.soma_gMax_kdrRL(self.soma_gMax_kdrRL_lb, self.soma_gMax_kdrRL_ub, skew)
        soma_gcamax_mAHP = self.soma_gcamax_mAHP(self.soma_gcamax_mAHP_lb, self.soma_gcamax_mAHP_ub, skew)
        soma_gkcamax_mAHP = self.soma_gkcamax_mAHP(self.soma_gkcamax_mAHP_lb, self.soma_gkcamax_mAHP_ub, skew)
        soma_taur_mAHP = self.soma_taur_mAHP(self.soma_taur_mAHP_lb, self.soma_taur_mAHP_ub, skew)
        soma_cm = self.soma_cm(self.soma_cm_lb, self.soma_cm_ub, skew)
        soma_gbar_gh = self.soma_ghbar_gh(self.soma_ghbar_gh_lb, self.soma_ghbar_gh_ub, skew)
        # Dendritic Parameters
        dend_all_L = self.dend_all_L(self.dend_all_L_lb, self.dend_all_L_ub, skew)
        dend_all_diam = self.dend_all_diam(self.dend_all_diam_lb, self.dend_all_diam_ub, skew)
        dend_all_g_pas = self.dend_all_g_pas(self.dend_all_g_pas_lb, self.dend_all_g_pas_ub, skew)
        dend_all_e_pas = self.dend_all_e_pas(self.dend_all_e_pas_lb, self.dend_all_e_pas_ub, skew)   
        dend_all_Ra = self.dend_all_Ra(self.dend_all_Ra_lb, self.dend_all_Ra_ub, skew)
        dend_all_cm = self.dend_all_cm(self.dend_all_cm_lb, self.dend_all_cm_ub, skew)
        dend_all_ghbar_gh = self.dend_all_ghbar_gh(self.dend_all_ghbar_gh_lb, self.dend_all_ghbar_gh_ub, skew)
        dend_1_gcabar_L_Ca_inact = self.dend_1_gcabar_L_Ca_inact(self.dend_1_gcabar_L_Ca_inact_lb, self.dend_1_gcabar_L_Ca_inact_ub, skew)
        dend_2_gcabar_L_Ca_inact = self.dend_2_gcabar_L_Ca_inact(self.dend_2_gcabar_L_Ca_inact_lb, self.dend_2_gcabar_L_Ca_inact_ub, skew)
        dend_3_gcabar_L_Ca_inact = self.dend_3_gcabar_L_Ca_inact(self.dend_3_gcabar_L_Ca_inact_lb, self.dend_3_gcabar_L_Ca_inact_ub, skew)
        dend_4_gcabar_L_Ca_inact = self.dend_4_gcabar_L_Ca_inact(self.dend_4_gcabar_L_Ca_inact_lb, self.dend_4_gcabar_L_Ca_inact_ub, skew)
        global_theta_m_L_Ca_inact = self.global_theta_m_L_Ca_inact(self.global_theta_m_L_Ca_inact_lb, self.global_theta_m_L_Ca_inact_ub, skew)
        global_theta_h_L_Ca_inact = self.global_theta_h_L_Ca_inact(self.global_theta_h_L_Ca_inact_lb, self.global_theta_h_L_Ca_inact_ub, skew)

        return soma_diam, soma_L, soma_g_pas, soma_e_pas, soma_gbar_na3rp, soma_gbar_naps, soma_gMax_kdrRL, \
    soma_gcamax_mAHP, soma_gkcamax_mAHP, soma_taur_mAHP, soma_cm, soma_gbar_gh, dend_all_L, dend_all_diam, \
    dend_all_g_pas, dend_all_e_pas, dend_all_Ra, dend_all_cm, dend_all_ghbar_gh, dend_1_gcabar_L_Ca_inact, \
    dend_2_gcabar_L_Ca_inact, dend_3_gcabar_L_Ca_inact, dend_4_gcabar_L_Ca_inact, global_theta_m_L_Ca_inact, global_theta_h_L_Ca_inact

if __name__ == '__main__':
    runner = Gather_Parameters()
    runner.__main__()    