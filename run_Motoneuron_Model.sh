#!/bin/bash -l

# Set the number of nodes
# SBATCH -N 1

# Set the number of tasks/cores per node required
#SBATCH -n 1

# Set the walltime of the job to 30 hours (format is hh:mm:ss)
#SBATCH -t 100:00:00

# E-mail on begin (b), abort (a), and end (end) of job
#SBATCH --mail-type=ALL

# E-mail address of recipient
#SBATCH --mail-user=john.fleming@ucdconnect.ie

# Specify the jobname
#SBATCH --job-name=run_Motoneuron_Example

# Specify the error and output file names
#SBATCH --error="Error_Motoneuron_Example-%j.out"
#SBATCH --output="Output_Motoneuron_Example-%j.out"

# Setup the environment
module load anaconda gcc openmpi/3.1.4
conda activate localpy36

# Change to model working directory
model_dir="${HOME}/Motoneuron_Model"

cd ${model_dir}

# Setup the model script
model_script=run_Spinal_Motoneuron_Pool_Model.py
model="${model_dir}/${model_script}"

# Command with minimum required arguments
run_command="python ${model} neuron"

# Run the simulation
eval $run_command

